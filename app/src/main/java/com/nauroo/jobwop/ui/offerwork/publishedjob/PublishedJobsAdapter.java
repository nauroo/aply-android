package com.nauroo.jobwop.ui.offerwork.publishedjob;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AddToFavRequestModel;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.jobdetail.JobDetailActivity;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohan M on 7/27/2017.
 */

public class PublishedJobsAdapter extends RecyclerView.Adapter<PublishedJobsAdapter.ViewHolder> {

    private Context context;
   List<AllJobsResponseModel> jobs;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView descriptionTextView,noOfApplicantTextView,remunerationTextView;
        ImageView jobImageView,favoriteImageView;
        RelativeLayout favoriteLayout;
        ProgressBar favoriteProgressBar;
        public ViewHolder(View v) {
            super(v);
            descriptionTextView=(TextView)v.findViewById(R.id.descriptionTextView);
            noOfApplicantTextView=(TextView)v.findViewById(R.id.noOfApplicantTextView);
            remunerationTextView=(TextView)v.findViewById(R.id.remunerationTextView);
            jobImageView=(ImageView) v.findViewById(R.id.jobsImageView);
            favoriteImageView=(ImageView)v.findViewById(R.id.favoriteImageView);
            favoriteLayout=(RelativeLayout)v.findViewById(R.id.favoriteLayout);
            favoriteProgressBar=(ProgressBar)v.findViewById(R.id.favoriteProgressIcon);
        }
    }

    public PublishedJobsAdapter(Context context,List<AllJobsResponseModel> jobs) {
        this.context=context;
        this.jobs=jobs;
    }

    @Override
    public PublishedJobsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.published_jobs_layout, parent, false);

       ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final PublishedJobsAdapter.ViewHolder holder, final int position) {
        holder.descriptionTextView.setText(jobs.get(position).getDescription());
        holder.remunerationTextView.setText(jobs.get(position).getRemuneration() );

        Picasso.with(context).load(jobs.get(position).getImageUrl()).fit().into(holder.jobImageView);
        holder.noOfApplicantTextView.setText(jobs.get(position).getApplicantsCount()+"");

        holder.favoriteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFavProgressBar(holder.favoriteProgressBar,holder.favoriteImageView);

//
                int isFav = Integer.valueOf(holder.favoriteImageView.getTag().toString());
                if (isFav == 1) {
                    holder.favoriteImageView.setTag(0);
                    hidFavProgressBar(holder.favoriteProgressBar, holder.favoriteImageView);
                    holder.favoriteImageView.setImageResource(R.drawable.fav_off);
                } else {
                    callAddToFavAPI(position, holder.favoriteImageView, holder.favoriteProgressBar);


                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToJobDetailActivity(position);
            }
        });
    }
    private void moveToJobDetailActivity(int position) {
        Intent intent = new Intent(context, JobDetailActivity.class);
        intent.putExtra(Constants.EXTRA_JOBS, jobs.get(position));
        intent.putExtra(Constants.EXTRA_SHOW_VIEW_APPLICANTS,true);
        intent.putExtra(Constants.EXTRA_SHOW_APPLY_TEXT,false);
        Constants.EXTRA_IS_ALREADY_APPLIED=jobs.get(position).isAlreadyApplied();
        context.startActivity(intent);
    }

    private void callAddToFavAPI(int position, final ImageView favoriteIcon, final ProgressBar favoriteProgressBar) {
        if (InternetAvailability.isNetworkAvailable(context)) {
            AddToFavRequestModel addToFavRequestModel = new AddToFavRequestModel();
            addToFavRequestModel.setId(jobs.get(position).getId());
            NetworkAdapter.getInstance(context).addToFavJob(addToFavRequestModel, new ResponseCallback<GenericResponse<String>>(context) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    favoriteIcon.setTag(1);
                    hidFavProgressBar(favoriteProgressBar, favoriteIcon);
                    favoriteIcon.setImageResource(R.drawable.fav_on);
                }

                @Override
                public void onFailure() {

                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    public void showFavProgressBar(ProgressBar progressBar, ImageView imageView) {
        progressBar.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
    }

    public void hidFavProgressBar(ProgressBar progressBar, ImageView imageView) {
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
    }
    @Override
    public int getItemCount() {
        return jobs.size();
    }
}