package com.nauroo.jobwop.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.nauroo.jobwop.R;


/**
 * Created by Mohan M on 2/17/2017.
 */

public class CustomButton extends Button {
    String customFont;

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context, attrs);

    }

    private void style(Context context, AttributeSet attrs) {


        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CustomFontTextView);
        int cf = a.getInteger(R.styleable.CustomFontTextView_font_type, 0);
        int fontName = 1;
        switch (cf) {
            case 1:
                fontName = R.string.helvetica;
                break;
            case 2:
                fontName = R.string.helvetica_light;
                break;


            default:
                break;
        }

        customFont = getResources().getString(fontName);

        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                "fonts/"+ customFont + ".ttf");
        setTypeface(tf);
        a.recycle();


    }
}
