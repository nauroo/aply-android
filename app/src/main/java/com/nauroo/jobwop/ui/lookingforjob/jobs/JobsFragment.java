package com.nauroo.jobwop.ui.lookingforjob.jobs;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PageRefreshEvent;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.jobsRecyclerView)
    RecyclerView jobsRecyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    List<AllJobsResponseModel> jobs;
    JobsAdapter jobsAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public JobsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JobsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobsFragment newInstance(String param1, String param2) {
        JobsFragment fragment = new JobsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jobs, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        getActivity().findViewById(R.id.actionTextView).setVisibility(View.GONE);
        initiallyGetAllJobs();
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAllJobs();
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        return view;
    }


    private void getAllJobs() {
        NetworkAdapter.getInstance(getContext()).getAllJobs(new ResponseCallback<GenericResponse<List<AllJobsResponseModel>>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<List<AllJobsResponseModel>> response) {
                setUpRecyclerView(response.getData());
                jobs = response.getData();
            }

            @Override
            public void onFailure() {


            }
        });
    }

    private void initiallyGetAllJobs() {
        showProgress();
        NetworkAdapter.getInstance(getContext()).getAllJobs(new ResponseCallback<GenericResponse<List<AllJobsResponseModel>>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<List<AllJobsResponseModel>> response) {
                setUpRecyclerView(response.getData());
                jobs = response.getData();
                hideProgress();
            }

            @Override
            public void onFailure() {


            }
        });
    }

    private void setUpRecyclerView(List<AllJobsResponseModel> jobs) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        jobsRecyclerView.setLayoutManager(layoutManager);
        jobsAdapter = new JobsAdapter(getContext(), jobs);
        jobsRecyclerView.setAdapter(jobsAdapter);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Subscribe
    public void onMessageEvent(PageRefreshEvent event) {
        if (event.getType() == 1) {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(true);
                    jobsAdapter.notifyItemChanged(i);
                }

            }
        } else {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(false);
                    jobsAdapter.notifyItemChanged(i);
                }

            }
        }
    }

    ;


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
