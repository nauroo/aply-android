package com.nauroo.jobwop.ui.profile;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.ApplicantsModel;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    ApplicantsModel applicantsModel;
    ;
    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;
    @BindView(R.id.nameTextView)
    CustomFontTextView nameTextView;
    @BindView(R.id.aboutTextView)
    CustomFontTextView aboutTextView;
    @BindView(R.id.interestsTextView)
    CustomFontTextView interestsTextView;
    @BindView(R.id.abilitiesTextView)
    CustomFontTextView abilitiesTextView;
    @BindView(R.id.mobileNumberTextView)
    CustomFontTextView mobileNumberTextView;
    @BindView(R.id.linkedinTextView)
    CustomFontTextView linkedinTextView;
    @BindView(R.id.facebookTextView)
    CustomFontTextView facebookTextView;
    @BindView(R.id.twitterTextView)
    CustomFontTextView twitterTextView;
    @BindView(R.id.skypeTextView)
    CustomFontTextView skypeTextView;
    @BindView(R.id.experienceTextView)
    CustomFontTextView experienceTextView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(ApplicantsModel param1) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            applicantsModel = getArguments().getParcelable(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        setData(applicantsModel);
        return view;
    }

    private void setData(ApplicantsModel applicantsModel) {

        if (applicantsModel.getFirstName() != null) {
            nameTextView.setText(applicantsModel.getFirstName() + " " + applicantsModel.getLastName());
        }
        if (applicantsModel.getAbility() != null) {
            abilitiesTextView.setText(applicantsModel.getAbility());
        }
        if (applicantsModel.getDescription()!=null){
            aboutTextView.setText(applicantsModel.getDescription());
        }
        if (applicantsModel.getExperience()!=null){
            experienceTextView.setText(applicantsModel.getExperience());
        }
        if (applicantsModel.getAbility() != null) {
            abilitiesTextView.setText(applicantsModel.getAbility());
        }
        if (applicantsModel.getInterest() != null) {
            interestsTextView.setText(applicantsModel.getInterest());
        }
        if (applicantsModel.getPhone() != null) {
            mobileNumberTextView.setText(applicantsModel.getPhone());
        }
        if (applicantsModel.getContactInfoList() != null) {
            for (int i = 0; i < applicantsModel.getContactInfoList().size(); i++) {
                if (applicantsModel.getContactInfoList().get(i).getType().equals("Skype")) {
                    skypeTextView.setText(applicantsModel.getContactInfoList().get(i).getInfo());
                }
                if (applicantsModel.getContactInfoList().get(i).getType().equals("Linkedin")) {
                    linkedinTextView.setText(applicantsModel.getContactInfoList().get(i).getInfo());
                }
                if (applicantsModel.getContactInfoList().get(i).getType().equals("Facebook")) {
                    facebookTextView.setText(applicantsModel.getContactInfoList().get(i).getInfo());
                }
                if (applicantsModel.getContactInfoList().get(i).getType().equals("Twitter")) {
                    twitterTextView.setText(applicantsModel.getContactInfoList().get(i).getInfo());
                }

            }
        }
        try {
            if (applicantsModel.getProfilePic() != null) {
                Picasso.with(getContext()).load(applicantsModel.getProfilePic()).into(profileImageView);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
