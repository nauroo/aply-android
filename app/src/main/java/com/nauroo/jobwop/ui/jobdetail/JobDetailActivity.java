package com.nauroo.jobwop.ui.jobdetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JobDetailActivity extends AppCompatActivity {

    AllJobsResponseModel allJobsResponseModel;
    @BindView(R.id.toolbar_icon)
    ImageView toolbarIcon;
    @BindView(R.id.toolbar_title)
    CustomFontTextView toolbarTitle;
    boolean showViewApplicant;
    boolean showApply;
    boolean isAlreadyApplied;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        allJobsResponseModel = getIntent().getParcelableExtra(Constants.EXTRA_JOBS);
        showViewApplicant=getIntent().getBooleanExtra(Constants.EXTRA_SHOW_VIEW_APPLICANTS,false);
        showApply=getIntent().getBooleanExtra(Constants.EXTRA_SHOW_APPLY_TEXT,false);
        isAlreadyApplied=Constants.EXTRA_IS_ALREADY_APPLIED;
        setTittle();
        loadJobDetailFragment();


    }

    private void setTittle() {
        try {
            toolbarTitle.setText(allJobsResponseModel.getName());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void loadJobDetailFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = JobDetailFragment.newInstance(allJobsResponseModel,showViewApplicant,showApply,isAlreadyApplied);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @OnClick(R.id.backIconLayout)
    public void onClick() {
        finish();
    }
}
