package com.nauroo.jobwop.ui.register;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.CityRequestModel;
import com.nauroo.jobwop.model.CountryModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.RegistrationRequestModel;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomEditText;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.ui.login.LoginActivity;
import com.nauroo.jobwop.ui.offerwork.postjob.CustomSpinnerAdapter1;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.emailEditText)
    CustomEditText emailEditText;
    @BindView(R.id.passwordEditText)
    CustomEditText passwordEditText;
    @BindView(R.id.confirmPasswordEditText)
    CustomEditText confirmPasswordEditText;
    @BindView(R.id.registerTextView)
    CustomFontTextView registerTextView;
    @BindView(R.id.loginTextView)
    CustomFontTextView loginTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.countrySpinner)
    Spinner countrySpinner;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    List<CountryModel> countryList;
    List<City> cityList;
    CityRequestModel city;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        getAllCountries();

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getCityOfParticularCountry(countrySpinner.getSelectedItemPosition()-1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (citySpinner.getSelectedItemPosition()!=0) {
                    CityRequestModel cityRequestModel=new CityRequestModel();
                    cityRequestModel.setId(cityList.get(citySpinner.getSelectedItemPosition() - 1).getId());
                    cityRequestModel.setCountry(cityList.get(citySpinner.getSelectedItemPosition() - 1).getCountry());
                    cityRequestModel.setCity(cityList.get(citySpinner.getSelectedItemPosition() - 1).getCity());
                    city=cityRequestModel;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void getCityOfParticularCountry(int i) {
        CityRequestModel cityRequestModel=new CityRequestModel();
        try {
            cityRequestModel.setCountry(countryList.get(i).getCountry());
            getCity(cityRequestModel);
        }catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }

    }

    private void getCity(CityRequestModel cityRequestModel) {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            showProgress();
            NetworkAdapter.getInstance(getContext()).getCityOfParticularCountry(cityRequestModel, new ResponseCallback<GenericResponse<List<City>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<City>> response) {
                    setUpCitySpinner(response);
                    hideProgress();
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_connectivity_is_not_available),Toast.LENGTH_SHORT).show();
        }

    }

    private void setUpCitySpinner(GenericResponse<List<City>> response) {
        cityList = response.getData();
        ArrayList<String> filterString = new ArrayList<>();
        for (int i = 0; i < response.getData().size(); i++) {
            if (i==0){
                filterString.add(getString(R.string.select_city));
            }
            filterString.add(response.getData().get(i).getCity());
        }
        CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
        citySpinner.setAdapter(customSpinnerAdapter);
       // citySpinner.setSelection(0);

    }

    private void getAllCountries() {

        if(InternetAvailability.isNetworkAvailable(getContext())){
            showProgress();
            NetworkAdapter.getInstance(getContext()).getAllCountry(new ResponseCallback<GenericResponse<List<CountryModel>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<CountryModel>> response) {
                    setUpCountrySpinner(response);
                    hideProgress();
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_connectivity_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpCountrySpinner(GenericResponse<List<CountryModel>> response) {

        countryList = response.getData();
        ArrayList<String> filterString = new ArrayList<>();
        for (int i = 0; i < response.getData().size(); i++) {
            if (i==0){
                filterString.add(getString(R.string.select_country));
            }
            filterString.add(response.getData().get(i).getCountry());
        }
        CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
        countrySpinner.setAdapter(customSpinnerAdapter);
        countrySpinner.setSelection(0);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.registerTextView, R.id.loginTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registerTextView:
                validateInputs();
                break;
            case R.id.loginTextView:
                loadLoginActivity();
                break;
        }
    }

    private void loadLoginActivity() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
    }


    private void validateInputs() {
        if (emailEditText.getText().length() == 0) {
            emailEditText.setError(getString(R.string.email_id_is_required));
        } else if (passwordEditText.length() == 0) {
            passwordEditText.setError(getString(R.string.password_is_required));
        } else if (confirmPasswordEditText.getText().length() == 0) {
            confirmPasswordEditText.setError(getString(R.string.confirm_password_is_required));
        } else if (!passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())) {
            Toast.makeText(getContext(), getString(R.string.password_and_confim_password_didt_match), Toast.LENGTH_SHORT).show();
        }else if (countrySpinner.getSelectedItemPosition()==0){
            Toast.makeText(getContext(),getString(R.string.please_select_country),Toast.LENGTH_SHORT).show();
        }else if (citySpinner.getSelectedItemPosition()==0){
            Toast.makeText(getContext(),getString(R.string.please_select_your_city),Toast.LENGTH_SHORT).show();
        }
        else {
            doRegister();
        }


    }

    private void doRegister() {
        showProgress();
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            RegistrationRequestModel registrationRequestModel = new RegistrationRequestModel();
            registrationRequestModel.setEmail(emailEditText.getText().toString());
            registrationRequestModel.setPassword(passwordEditText.getText().toString());
            registrationRequestModel.setCity(city);
            NetworkAdapter.getInstance(getContext()).registerUser(registrationRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    hideProgress();
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        Toast.makeText(getContext(), getString(R.string.please_login_to_continue), Toast.LENGTH_LONG).show();
                        loadLoginActivity();
                    } else if (response.getStatus() == Constants.USER_ALREADY_EXIST) {
                        Toast.makeText(getContext(), getString(R.string.you_have_already_register_with_this_mail_id), Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure() {
                    hideProgress();
                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_LONG).show();
        }

    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
