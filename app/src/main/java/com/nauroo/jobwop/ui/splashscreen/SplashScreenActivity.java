package com.nauroo.jobwop.ui.splashscreen;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.User;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.ui.home.HomeActivity;
import com.nauroo.jobwop.ui.login.LoginActivity;
import com.nauroo.jobwop.ui.register.RegisterActivity;
import com.nauroo.jobwop.ui.splashscreen.intro.IntroAdapter;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.UserUtils;
import com.nauroo.jobwop.utils.Utils;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashScreenActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.viewpagerIndicator)
    CirclePageIndicator viewpagerIndicator;
    @BindView(R.id.loginTextView)
    CustomFontTextView loginTextView;
    @BindView(R.id.registerTextView)
    CustomFontTextView registerTextView;
    @BindView(R.id.activity_splash_screen)
    RelativeLayout activitySplashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkIsUserIsAlreadyLoggedIn();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        setUpViewPager();
    }

    private void checkIsUserIsAlreadyLoggedIn() {
        if(Utils.isLoggedIn(getApplicationContext())) {
            getProfileDetails();
            loadHomeActivity();

        }
    }

    private void getProfileDetails() {
        NetworkAdapter.getInstance(getApplicationContext()).getProfile(new ResponseCallback<GenericResponse<User>>(getApplicationContext()) {
            @Override
            public void onResponse(GenericResponse<User> response) {
                if(response!=null&&response.getStatus()== Constants.RESPONSE_SUCCESS) {
                    UserUtils.setUserProfile(getApplicationContext(),response.getData());
                    Utils.setEmailId(getApplicationContext(),Utils.getEmailId(getApplicationContext()));
                    Utils.setUserPassword(getApplicationContext(),Utils.getUserPassword(getApplicationContext()));
                }

            }

            @Override
            public void onFailure() {

            }
        });

    }

    private void loadHomeActivity() {
        Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void setUpViewPager() {
        IntroAdapter introAdapter = new IntroAdapter(getSupportFragmentManager());
        viewPager.setAdapter(introAdapter);
        viewpagerIndicator.setViewPager(viewPager);
    }

    @OnClick({R.id.loginTextView, R.id.registerTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginTextView:
                loadLoginActivity();
                break;
            case R.id.registerTextView:
                loadRegisterActivity();
                break;
        }
    }

    private void loadRegisterActivity() {
        Intent intent=new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    private void loadLoginActivity() {
        Intent intent=new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}
