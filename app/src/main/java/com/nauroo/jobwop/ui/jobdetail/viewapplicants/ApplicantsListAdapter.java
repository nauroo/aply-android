package com.nauroo.jobwop.ui.jobdetail.viewapplicants;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.ApplicantsModel;
import com.nauroo.jobwop.ui.profile.ProfileActivity;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.TimeAgo;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohan M on 7/27/2017.
 */

public class ApplicantsListAdapter extends RecyclerView.Adapter<ApplicantsListAdapter.ViewHolder> {

    private Context context;
    List<ApplicantsModel> userList;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView jobImageView;
        TextView tittleTextView,timeTextView,viewProfileTextView;
        public ViewHolder(View v) {
            super(v);
            jobImageView=(ImageView)v.findViewById(R.id.jobImageView);
            tittleTextView=(TextView)v.findViewById(R.id.tittleTextView);
            timeTextView=(TextView)v.findViewById(R.id.timeTextView);
            viewProfileTextView=(TextView)v.findViewById(R.id.viewProfileTextView);

        }
    }

    public ApplicantsListAdapter(Context context, List<ApplicantsModel> userList) {
        this.context=context;
        this.userList=userList;
    }

    @Override
    public ApplicantsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.applicants_list_layout, parent, false);

      ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ApplicantsListAdapter.ViewHolder holder, final int position) {
        if (userList.get(position).getProfilePic()!=null){
            Picasso.with(context).load(userList.get(position).getProfilePic()).fit().into(holder.jobImageView);
        }
        String tittle="<u>"+userList.get(position).getFirstName()+" "+userList.get(position).getLastName()+"</u>"+" "+
                context.getString(R.string.applied_for_the_job);
        holder.tittleTextView.setText(Html.fromHtml(tittle));
        long now = System.currentTimeMillis();
        holder.timeTextView.setText(TimeAgo.getTimeAgo(now,context));
        holder.viewProfileTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,ProfileActivity.class);
                intent.putExtra(Constants.EXTRA_USER,userList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}