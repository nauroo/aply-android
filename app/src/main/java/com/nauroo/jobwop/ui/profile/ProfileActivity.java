package com.nauroo.jobwop.ui.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.ApplicantsModel;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    ApplicantsModel applicantsModel;
    @BindView(R.id.backIconLayout)
    RelativeLayout backIconLayout;
    @BindView(R.id.toolbar_title)
    CustomFontTextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        applicantsModel = getIntent().getParcelableExtra(Constants.EXTRA_USER);
        loadJobDetailFragment();

    }


    private void loadJobDetailFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = ProfileFragment.newInstance(applicantsModel);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @OnClick(R.id.backIconLayout)
    public void onClick() {
        finish();
    }
}
