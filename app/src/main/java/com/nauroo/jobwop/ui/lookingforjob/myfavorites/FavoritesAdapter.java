package com.nauroo.jobwop.ui.lookingforjob.myfavorites;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AddJobRequestModel;
import com.nauroo.jobwop.model.AddToFavRequestModel;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PageRefreshEvent;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.jobdetail.JobDetailActivity;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Mohan M on 7/26/2017.
 */

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> {

    private Context context;
    List<AllJobsResponseModel> jobs;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView jobImageView, favoriteIcon;
        TextView jobTittleTextView, remuneartionTextView, jobDescriptionTextView, applyTextView;
        ProgressBar favoriteProgressBar;
        RelativeLayout favoriteLayout;

        public ViewHolder(View v) {
            super(v);
            jobImageView = (ImageView) v.findViewById(R.id.jobImageView);
            favoriteIcon = (ImageView) v.findViewById(R.id.favoriteImageView);
            jobTittleTextView = (TextView) v.findViewById(R.id.jobTittleTextView);
            remuneartionTextView = (TextView) v.findViewById(R.id.remunerationTextView);
            jobDescriptionTextView = (TextView) v.findViewById(R.id.jobDescriptionTextView);
            applyTextView = (TextView) v.findViewById(R.id.applyTextView);
            favoriteProgressBar = (ProgressBar) v.findViewById(R.id.favoriteProgressIcon);
            favoriteLayout = (RelativeLayout) v.findViewById(R.id.favoriteLayout);


        }
    }

    public FavoritesAdapter(Context context, List<AllJobsResponseModel> jobs) {
        this.context = context;
        this.jobs = jobs;
    }

    @Override
    public FavoritesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorites_layout, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final FavoritesAdapter.ViewHolder holder, final int position) {
        Picasso.with(context).load(jobs.get(position).getImageUrl()).fit().into(holder.jobImageView);
        holder.jobTittleTextView.setText(jobs.get(position).getName());
        holder.jobDescriptionTextView.setText(jobs.get(position).getDescription());
        holder.remuneartionTextView.setText(jobs.get(position).getRemuneration());
        if (jobs.get(position).isFavorite()) {
            holder.favoriteIcon.setImageResource(R.drawable.fav_on);
            holder.favoriteIcon.setTag(1);
        } else {
            holder.favoriteIcon.setImageResource(R.drawable.fav_off);
            holder.favoriteIcon.setTag(0);
        }
        if (jobs.get(position).isAlreadyApplied()) {
            holder.applyTextView.setTag(1);
            holder.applyTextView.setText(context.getString(R.string.applied));
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.applyTextView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.un_apply_button_background));
            } else {
                holder.applyTextView.setBackground(context.getResources().getDrawable(R.drawable.un_apply_button_background));
            }
        } else {
            holder.applyTextView.setTag(0);
            holder.applyTextView.setText(context.getString(R.string.apply));
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.applyTextView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.apply_button_background));
            } else {
                holder.applyTextView.setBackground(context.getResources().getDrawable(R.drawable.apply_button_background));
            }

        }

        holder.favoriteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showFavProgressBar(holder.favoriteProgressBar, holder.favoriteIcon);
                int isFav = Integer.valueOf(holder.favoriteIcon.getTag().toString());
                if (isFav == 1) {
                    callRemoveFavAPI(position, holder.favoriteIcon, holder.favoriteProgressBar);
                } else {
                    callAddToFavAPI(position, holder.favoriteIcon, holder.favoriteProgressBar);


                }

            }
        });

        holder.applyTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int isApplied = Integer.valueOf(holder.applyTextView.getTag().toString());
                if (isApplied == 0) {
                    addUserToJob(holder.applyTextView, position);
                } else {
                    new AlertDialog.Builder(context)
                            .setMessage(context.getString(R.string.are_you_sure_want_unapply_job))
                            .setCancelable(false)
                            .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    removeUserToJob(holder.applyTextView, position);
                                }
                            })
                            .setNegativeButton(context.getString(R.string.no), null)
                            .show();

                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToJobDetailActivity(position);
            }
        });
    }

    private void removeUserToJob(final TextView applyTextView, final int position) {
        applyTextView.setText(context.getString(R.string.loading));
        if (InternetAvailability.isNetworkAvailable(context)) {
            AddJobRequestModel addJobRequestModel = new AddJobRequestModel();
            addJobRequestModel.setId(jobs.get(position).getId());
            NetworkAdapter.getInstance(context).removeJob(addJobRequestModel, new ResponseCallback<GenericResponse<String>>(context) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        updateApplyTextViewForUnapply(applyTextView);
                        jobs.get(position).setAlreadyApplied(false);
                        updateApplyTextViewAsApplied();
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateApplyTextViewForUnapply(TextView applyTextView) {
        applyTextView.setTag(0);
        applyTextView.setText(context.getString(R.string.apply));
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            applyTextView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.apply_button_background));
        } else {
            applyTextView.setBackground(context.getResources().getDrawable(R.drawable.apply_button_background));
        }
    }

    private void callAddToFavAPI(int position, final ImageView favoriteIcon, final ProgressBar favoriteProgressBar) {
        if (InternetAvailability.isNetworkAvailable(context)) {
            AddToFavRequestModel addToFavRequestModel = new AddToFavRequestModel();
            addToFavRequestModel.setId(jobs.get(position).getId());
            NetworkAdapter.getInstance(context).addToFavJob(addToFavRequestModel, new ResponseCallback<GenericResponse<String>>(context) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    favoriteIcon.setTag(1);
                    hidFavProgressBar(favoriteProgressBar, favoriteIcon);
                    favoriteIcon.setImageResource(R.drawable.fav_on);
                }

                @Override
                public void onFailure() {

                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void callRemoveFavAPI(final int position, final ImageView favoriteIcon, final ProgressBar favoriteProgressBar) {
        if (InternetAvailability.isNetworkAvailable(context)) {
            AddToFavRequestModel addToFavRequestModel = new AddToFavRequestModel();
            addToFavRequestModel.setId(jobs.get(position).getId());
            NetworkAdapter.getInstance(context).addToFavJob(addToFavRequestModel, new ResponseCallback<GenericResponse<String>>(context) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    favoriteIcon.setTag(0);
                    hidFavProgressBar(favoriteProgressBar, favoriteIcon);
                    favoriteIcon.setImageResource(R.drawable.fav_off);
                    removeAt(position);
                }

                @Override
                public void onFailure() {

                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToJobDetailActivity(int position) {
        Intent intent = new Intent(context, JobDetailActivity.class);
        intent.putExtra(Constants.EXTRA_JOBS, jobs.get(position));
        intent.putExtra(Constants.EXTRA_SHOW_VIEW_APPLICANTS, false);
        intent.putExtra(Constants.EXTRA_SHOW_APPLY_TEXT, true);
        Constants.EXTRA_IS_ALREADY_APPLIED=jobs.get(position).isAlreadyApplied();
        context.startActivity(intent);
    }

    private void addUserToJob(final TextView applyTextView, final int position) {
        if (InternetAvailability.isNetworkAvailable(context)) {
            applyTextView.setText(context.getString(R.string.loading));
            AddJobRequestModel addJobRequestModel = new AddJobRequestModel();
            addJobRequestModel.setId(jobs.get(position).getId());
            NetworkAdapter.getInstance(context).addJobToUser(addJobRequestModel, new ResponseCallback<GenericResponse<String>>(context) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        updateApplyTextView(applyTextView);
                        jobs.get(position).setAlreadyApplied(true);
                        updateApplyTextViewAsApply();
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(context, context.getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }

    }

    private void updateApplyTextView(TextView applyTextView) {
        applyTextView.setTag(1);
        applyTextView.setText(context.getString(R.string.applied));
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            applyTextView.setBackgroundDrawable( context.getResources().getDrawable(R.drawable.un_apply_button_background) );
        } else {
            applyTextView.setBackground( context.getResources().getDrawable(R.drawable.un_apply_button_background));
        }
    }

    public void showFavProgressBar(ProgressBar progressBar, ImageView imageView) {
        progressBar.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
    }

    public void hidFavProgressBar(ProgressBar progressBar, ImageView imageView) {
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
    }

    private void updateApplyTextViewAsApplied() {
        EventBus.getDefault().post(new PageRefreshEvent(1, PageRefreshEvent.ITEM_ID));


    }

    private void updateApplyTextViewAsApply() {
        EventBus.getDefault().post(new PageRefreshEvent(0, PageRefreshEvent.ITEM_ID));


    }

    public void removeAt(int position) {
        jobs.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, jobs.size());
    }

    @Override
    public int getItemCount() {
        return jobs.size();
    }
}
