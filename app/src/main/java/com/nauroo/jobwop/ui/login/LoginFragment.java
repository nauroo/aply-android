package com.nauroo.jobwop.ui.login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AuthenticationResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.LoginUserModel;
import com.nauroo.jobwop.model.User;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomEditText;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.ui.home.HomeActivity;
import com.nauroo.jobwop.ui.register.RegisterActivity;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.nauroo.jobwop.utils.UserUtils;
import com.nauroo.jobwop.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.emailEditText)
    CustomEditText emailEditText;
    @BindView(R.id.passwordEditText)
    CustomEditText passwordEditText;
    @BindView(R.id.loginTextView)
    CustomFontTextView loginTextView;
    @BindView(R.id.registerTextView)
    CustomFontTextView registerTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.loginTextView, R.id.registerTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginTextView:
                doLogin();
                break;
            case R.id.registerTextView:
                loadRegistrationActivity();
                break;
        }
    }

    private void loadRegistrationActivity() {
        Intent intent = new Intent(getContext(), RegisterActivity.class);
        startActivity(intent);
    }

    private void doLogin() {
        if (emailEditText.getText().length() == 0) {
            emailEditText.setError(getString(R.string.email_id_is_required));
        } else if (passwordEditText.getText().length() == 0) {
            passwordEditText.setError(getString(R.string.password_is_required));
        } else {
            callLoginAPI();
        }

    }

    private void callLoginAPI() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            //Call API
            showProgress();
            LoginUserModel loginUserModel = new LoginUserModel();
            loginUserModel.setPassword(passwordEditText.getText().toString());
            loginUserModel.setUsername(emailEditText.getText().toString());
            loginUserModel.setGrandTypePassword("password");
            NetworkAdapter.getInstance(getActivity()).login(loginUserModel, new Callback<AuthenticationResponseModel>() {
                @Override
                public void onResponse(Call<AuthenticationResponseModel> call, Response<AuthenticationResponseModel> response) {
                    if (!isAdded()) return;
                    if (response != null && response.body() != null) {

                        Utils.setAccessToken(getActivity(), response.body().getAccessToken());
                        Utils.setRefreshToken(getActivity(), response.body().getRefresh_token());
                        hideProgress();
                        getProfileDetails();
                    } else {
                        hideProgress();
                        Toast.makeText(getContext(), getString(R.string.username_or_password_is_incorrect), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AuthenticationResponseModel> call, Throwable t) {
                    hideProgress();

                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_LONG).show();

        }
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void getProfileDetails() {
        NetworkAdapter.getInstance(getContext()).getProfile(new ResponseCallback<GenericResponse<User>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<User> response) {
                if(response!=null&&response.getStatus()==Constants.RESPONSE_SUCCESS) {
                    UserUtils.setUserProfile(getContext(),response.getData());
                    Utils.setEmailId(getContext(),emailEditText.getText().toString());
                    Utils.setUserPassword(getContext(),passwordEditText.getText().toString());
                    loadHomeActivity();
                }

            }

            @Override
            public void onFailure() {

            }
        });

    }

    private void loadHomeActivity() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
