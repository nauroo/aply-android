package com.nauroo.jobwop.ui.lookingforjob.search;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nauroo.jobwop.R;

import java.util.ArrayList;

/**
 * Created by Mohan M on 7/26/2017.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private Context activity;
    private ArrayList<String> data;
    LayoutInflater inflater;

    /*************  CustomAdapter Constructor *****************/
    public CustomSpinnerAdapter(
            Context activitySpinner,
            int textViewResourceId,
            ArrayList<String> objects
    )
    {
        super(activitySpinner, textViewResourceId, objects);

        /********** Take passed values **********/
        activity = activitySpinner;
        data     = objects;
        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.search_filter_layout, parent, false);

        /***** Get each Model object from Arraylist ********/

        TextView label  = (TextView)row.findViewById(R.id.text);


        // Set values for spinner each row
        label.setText(data.get(position));
        Display display =  display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int _width = size.x;

        row.setMinimumWidth(_width);;
        return row;
    }
}