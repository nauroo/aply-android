package com.nauroo.jobwop.ui.offerwork.postjob;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.KeywordRequest;
import com.nauroo.jobwop.model.PostJobRequestModel;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomEditText;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostJobFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostJobFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CODE_PICKER = 100;
    @BindView(R.id.selectImageLayout)
    RelativeLayout selectImageLayout;
    @BindView(R.id.jobTittleEditText)
    CustomEditText jobTittleEditText;
    @BindView(R.id.descriptionEditText)
    CustomEditText descriptionEditText;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    @BindView(R.id.remunerationSpinner)
    Spinner remunerationSpinner;
    @BindView(R.id.periodEditText)
    CustomEditText periodEditText;
    @BindView(R.id.keywordEditText)
    CustomEditText keywordEditText;
    @BindView(R.id.jobImageView)
    ImageView jobImageView;
    TextView publishTextView;
    @BindView(R.id.remunerationEditText)
    CustomEditText remunerationEditText;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    File imgFile;
    List<City> cityList;
    List<String> remuneration;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.workingHoursEditText)
    CustomEditText workingHoursEditText;
    @BindView(R.id.addImageTextView)
    CustomFontTextView addImageTextView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PostJobFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PostJobFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PostJobFragment newInstance(String param1, String param2) {
        PostJobFragment fragment = new PostJobFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_job, container, false);
        ButterKnife.bind(this, view);
        showPublishTextView();
        setUpCitySpinner();
        setUpPeriodSpinner();
        publishTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateInputs();
            }
        });
        descriptionEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.descriptionEditText) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction()&MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        return view;

    }

    private void validateInputs() {
        if (imgFile == null) {
            Toast.makeText(getContext(), getString(R.string.please_upload_job_image), Toast.LENGTH_SHORT).show();
        } else if (jobTittleEditText.getText().length() == 0) {
            jobTittleEditText.setError(getString(R.string.job_tittle_is_required));
        } else if (descriptionEditText.getText().length() == 0) {
            descriptionEditText.setError(getString(R.string.job_description_is_required));

        } else if (remunerationEditText.getText().length() == 0) {
            remunerationEditText.setError(getString(R.string.remuneration_is_required));
        } else if (periodEditText.getText().length() == 0) {
            periodEditText.setError(getString(R.string.periodicity_is_required));
        } else if (keywordEditText.getText().length() == 0) {
            keywordEditText.setError(getString(R.string.keyword_is_required));
        } else if (workingHoursEditText.length() == 0) {
            workingHoursEditText.setError(getString(R.string.working_hours_field_is_required));
        } else {

            new AlertDialog.Builder(getActivity())
                    .setMessage(getResources().getString(R.string.are_you_sure_want_to_post_this_job))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            callPostJobApi();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), null)
                    .show();
        }

    }

    private void callPostJobApi() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgress();
            PostJobRequestModel postJobRequestModel = new PostJobRequestModel();
            postJobRequestModel.setName(jobTittleEditText.getText().toString());
            postJobRequestModel.setDescription(descriptionEditText.getText().toString());
            String period = remuneration.get(remunerationSpinner.getSelectedItemPosition()).replace("Per", "").trim();
            String remuneration = remunerationEditText.getText().toString() + " / " + period;
            postJobRequestModel.setRemuneration(remuneration);
            postJobRequestModel.setPeriodicity(periodEditText.getText().toString());
            City city = cityList.get(citySpinner.getSelectedItemPosition());
            postJobRequestModel.setCity(city);
            List<KeywordRequest> keywordRequestArrayList = new ArrayList<>();
            String[] keyword = keywordEditText.getText().toString().split(",");
            for (String aKeyword : keyword) {
                KeywordRequest keywordRequest = new KeywordRequest();
                keywordRequest.setKeyword(aKeyword);
                keywordRequestArrayList.add(keywordRequest);
            }
            postJobRequestModel.setKeywords(keywordRequestArrayList);
            postJobRequestModel.setSchedule(workingHoursEditText.getText().toString());

            NetworkAdapter.getInstance(getContext()).postJob(imgFile, postJobRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        Toast.makeText(getContext(), getString(R.string.your_job_is_successfully_posted), Toast.LENGTH_LONG).show();
                        resetAllData();
                        hideProgress();
                    }
                }


                @Override
                public void onFailure() {
                    Toast.makeText(getContext(), getString(R.string.some_thing_went_wrong), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_LONG).show();
        }
    }

    private void resetAllData() {
        scrollView.scrollTo(0, 0);
        imgFile = null;
        jobImageView.setImageResource(R.drawable.add_image);
        jobTittleEditText.setText(null);
        descriptionEditText.setText(null);
        citySpinner.setSelection(0);
        remunerationEditText.setText(null);
        remunerationSpinner.setSelection(0);
        keywordEditText.setText(null);
        periodEditText.setText(null);
        workingHoursEditText.setText(null);

    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }


    private void showPublishTextView() {
        publishTextView = (TextView) getActivity().findViewById(R.id.actionTextView);
        publishTextView.setVisibility(View.VISIBLE);
        publishTextView.setText(getString(R.string.publish));
    }

    private void setUpCitySpinner() {
        showProgress();
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            NetworkAdapter.getInstance(getContext()).getCity(new ResponseCallback<GenericResponse<List<City>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<City>> response) {
                    cityList = response.getData();
                    ArrayList<String> filterString = new ArrayList<>();
                    for (int i = 0; i < response.getData().size(); i++) {
                        filterString.add(response.getData().get(i).getCity());
                    }
                    CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
                    citySpinner.setAdapter(customSpinnerAdapter);
                    citySpinner.setSelection(0);
                    hideProgress();
                }

                @Override
                public void onFailure() {


                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }

    }


    private void setUpPeriodSpinner() {
        ArrayList<String> filterString = new ArrayList<>();
        filterString.add(getString(R.string.per_hour));
        filterString.add(getString(R.string.per_day));
        filterString.add(getString(R.string.per_week));
        filterString.add(getString(R.string.per_two_week));
        filterString.add(getString(R.string.per_month));
        remuneration = filterString;
        CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
        remunerationSpinner.setAdapter(customSpinnerAdapter);
        remunerationSpinner.setSelection(0);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.selectImageLayout)
    public void onClick() {
        PickImage();
    }

    private void PickImage() {

        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .multi() // multi mode (default mode)
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            imgFile = new File(images.get(0).getPath());
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                jobImageView.setImageBitmap(myBitmap);
                addImageTextView.setVisibility(View.GONE);

            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
