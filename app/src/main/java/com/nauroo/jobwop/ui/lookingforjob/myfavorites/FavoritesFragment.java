package com.nauroo.jobwop.ui.lookingforjob.myfavorites;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PageRefreshEvent;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.utils.InternetAvailability;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoritesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.favoritesRecyclerView)
    RecyclerView favoritesRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.errorMessageTextView)
    CustomFontTextView errorMessageTextView;
    List<AllJobsResponseModel> jobs;
    FavoritesAdapter favoritesAdapter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoritesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritesFragment newInstance(String param1, String param2) {
        FavoritesFragment fragment = new FavoritesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        getActivity().findViewById(R.id.actionTextView).setVisibility(View.GONE);

        getAllFavorites();
        return view;
    }

    private void getAllFavorites() {
        showProgress();
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            NetworkAdapter.getInstance(getContext()).getFavorites(new ResponseCallback<GenericResponse<List<AllJobsResponseModel>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<AllJobsResponseModel>> response) {
                    hideProgress();
                    setUpFavoritesRecyclerView(response.getData());
                }

                @Override
                public void onFailure() {
                    hideProgress();
                    showErrorMessage();
                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_LONG).show();
        }
    }

    private void setUpFavoritesRecyclerView(List<AllJobsResponseModel> allJobsResponseModelList) {
        jobs=allJobsResponseModelList;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        favoritesRecyclerView.setLayoutManager(layoutManager);
        favoritesAdapter= new FavoritesAdapter(getContext(), allJobsResponseModelList);
        favoritesRecyclerView.setAdapter(favoritesAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void showErrorMessage(){
        errorMessageTextView.setVisibility(View.VISIBLE);
    }
    public void hideErrorMessage(){
        errorMessageTextView.setVisibility(View.GONE);
    }


    @Subscribe
    public void onMessageEvent(PageRefreshEvent event) {
        if (event.getType() == 1) {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(true);
                    favoritesAdapter.notifyItemChanged(i);
                }

            }
        } else {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(false);
                    favoritesAdapter.notifyItemChanged(i);
                }

            }
        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
