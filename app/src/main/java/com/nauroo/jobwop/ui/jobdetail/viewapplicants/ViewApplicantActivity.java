package com.nauroo.jobwop.ui.jobdetail.viewapplicants;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewApplicantActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_icon)
    ImageView toolbarIcon;
    @BindView(R.id.toolbar_title)
    CustomFontTextView toolbarTitle;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_applicant);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        id=getIntent().getStringExtra(Constants.EXTRA_PUBLISHED_JOB_ID);
        loadApplicantsListFragment();

    }

    private void loadApplicantsListFragment() {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = ApplicantsListFragment.newInstance(id);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }


    @OnClick(R.id.backIconLayout)
    public void onClick() {
        finish();
    }
}
