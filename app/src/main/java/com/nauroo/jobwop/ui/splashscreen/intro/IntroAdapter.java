package com.nauroo.jobwop.ui.splashscreen.intro;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by Mohan M on 2/15/2017.
 */

public class IntroAdapter extends FragmentPagerAdapter {
    public IntroAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FirstIntroFragment();
            case 1:
                return new SecondIntroFragment();
            case 2:
                return new ThirdFragment();
            case 3:
              return null;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
