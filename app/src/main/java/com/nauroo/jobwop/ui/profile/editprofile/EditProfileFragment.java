package com.nauroo.jobwop.ui.profile.editprofile;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.CityRequestModel;
import com.nauroo.jobwop.model.ContactInfoRequest;
import com.nauroo.jobwop.model.CountryModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.UpdateProfileRequestModel;
import com.nauroo.jobwop.model.User;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomEditText;
import com.nauroo.jobwop.ui.offerwork.postjob.CustomSpinnerAdapter1;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.nauroo.jobwop.utils.UserUtils;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CODE_PICKER = 100;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.nameEditText)
    CustomEditText nameEditText;
    @BindView(R.id.ageEditText)
    CustomEditText ageEditText;
    @BindView(R.id.emailIdEditText)
    CustomEditText emailIdEditText;
    @BindView(R.id.mobileNumberEditText)
    CustomEditText mobileNumberEditText;
    @BindView(R.id.aboutMeEditText)
    CustomEditText aboutMeEditText;
    @BindView(R.id.interestsEditText)
    CustomEditText interestsEditText;
    @BindView(R.id.abilitiesEditText)
    CustomEditText abilitiesEditText;
    @BindView(R.id.skypeEditText)
    CustomEditText skypeEditText;
    @BindView(R.id.linkedInEditText)
    CustomEditText linkedInEditText;
    @BindView(R.id.facebookEditText)
    CustomEditText facebookEditText;
    @BindView(R.id.twitterEditText)
    CustomEditText twitterEditText;
    TextView actionTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.containerLayout)
    LinearLayout containerLayout;
    volatile int tag;
    @BindView(R.id.citySpinner)
    Spinner citySpinner;
    @BindView(R.id.experienceEditText)
    CustomEditText experienceEditText;
    @BindView(R.id.countrySpinner)
    Spinner countrySpinner;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    List<CountryModel> countryList;
    List<City> cityList;
    CityRequestModel city;
    private OnFragmentInteractionListener mListener;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        actionTextView = (TextView) getActivity().findViewById(R.id.actionTextView);
        ButterKnife.bind(this, view);


        actionTextView.setVisibility(View.VISIBLE);
        actionTextView.setText(getString(R.string.edit));
        actionTextView.setTag(0);
        hideTopLayout();
        showProgress();
        getProfileDetails();
        setViewsAsNonEditable();

        getAllCities();
        getAllCountries();
        disableCountryAndCitySpinner();

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy/MM/dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                ageEditText.setText(sdf.format(myCalendar.getTime()));
            }

        };


        ageEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getCityOfParticularCountry(countrySpinner.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CityRequestModel cityRequestModel = new CityRequestModel();
                cityRequestModel.setId(cityList.get(citySpinner.getSelectedItemPosition()).getId());
                cityRequestModel.setCountry(cityList.get(citySpinner.getSelectedItemPosition()).getCountry());
                cityRequestModel.setCity(cityList.get(citySpinner.getSelectedItemPosition()).getCity());
                city = cityRequestModel;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        aboutMeEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.aboutMeEditText) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        interestsEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.interestsEditText) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        abilitiesEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.abilitiesEditText) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        experienceEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // TODO Auto-generated method stub
                if (view.getId() == R.id.experienceEditText) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        actionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tag = Integer.valueOf(actionTextView.getTag().toString());
                if (tag == 1) {
                    actionTextView.setTag(0);
                    setViewsAsNonEditable();
                    actionTextView.setText(getString(R.string.edit));
                    disableCountryAndCitySpinner();
                    updateUserInfo();
                } else {
                    tag = 1;
                    setViewsAsEditable();
                    actionTextView.setText(getString(R.string.save));
                    actionTextView.setTag(1);
                    enableCountryAndCitySpinner();

                }

            }
        });

        return view;
    }

    private void disableCountryAndCitySpinner() {
        countrySpinner.setEnabled(false);
        citySpinner.setEnabled(false);
    }

    private void enableCountryAndCitySpinner() {
        countrySpinner.setEnabled(true);
        citySpinner.setEnabled(true);
    }

    private void enableCountrySpinner() {
        countrySpinner.setEnabled(true);
    }

    private void disableCountrySpinner() {
        countrySpinner.setEnabled(false);
    }

    private void enableCitySpinner() {
        citySpinner.setEnabled(true);
    }

    private void disableCitySpinner() {
        citySpinner.setEnabled(false);
    }

    private void getAllCities() {
        NetworkAdapter.getInstance(getContext()).getCity(new ResponseCallback<GenericResponse<List<City>>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<List<City>> response) {
                setUpCitySpinner(response);
            }


            @Override
            public void onFailure() {

            }
        });
    }


    private void getCityOfParticularCountry(int i) {
        CityRequestModel cityRequestModel = new CityRequestModel();
        try {
            cityRequestModel.setCountry(countryList.get(i).getCountry());
            getCity(cityRequestModel);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

    }

    private void getCity(CityRequestModel cityRequestModel) {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgress();
            NetworkAdapter.getInstance(getContext()).getCityOfParticularCountry(cityRequestModel, new ResponseCallback<GenericResponse<List<City>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<City>> response) {
                    setUpCitySpinner(response);
                    hideProgress();
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }

    }

    private void setUpCitySpinner(GenericResponse<List<City>> response) {
        cityList = response.getData();
        int selectedPosition = 0;
        ArrayList<String> filterString = new ArrayList<>();
        for (int i = 0; i < response.getData().size(); i++) {
            filterString.add(response.getData().get(i).getCity());
            if (response.getData().get(i).getId().equals(UserUtils.getUserProfile(getContext()).getCity().getId())) {
                selectedPosition = i;
            }
        }
        CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
        citySpinner.setAdapter(customSpinnerAdapter);
        citySpinner.setSelection(selectedPosition);

    }

    private void setUpCountrySpinner(GenericResponse<List<CountryModel>> response) {

        countryList = response.getData();
        int selectedPosition = 0;
        ArrayList<String> filterString = new ArrayList<>();
        for (int i = 0; i < response.getData().size(); i++) {
            filterString.add(response.getData().get(i).getCountry());
            if (response.getData().get(i).getCountry().equals(UserUtils.getUserProfile(getContext()).getCity().getCountry())) {
                selectedPosition = i;
            }

        }
        CustomSpinnerAdapter1 customSpinnerAdapter = new CustomSpinnerAdapter1(getActivity(), R.layout.spinner_drop_down_layout, filterString);
        countrySpinner.setAdapter(customSpinnerAdapter);
        countrySpinner.setSelection(selectedPosition);
    }

    private void updateUserInfo() {
        UpdateProfileRequestModel updateProfileRequestModel = new UpdateProfileRequestModel();
        String[] names = nameEditText.getText().toString().split(" ");
        if (names.length == 2) {
            updateProfileRequestModel.setFirstName(names[0]);
            updateProfileRequestModel.setLastName(names[1]);
        } else {
            updateProfileRequestModel.setFirstName(names[0]);
        }
        updateProfileRequestModel.setPhone(mobileNumberEditText.getText().toString());
        updateProfileRequestModel.setDescription(aboutMeEditText.getText().toString());

        updateProfileRequestModel.setAbility(abilitiesEditText.getText().toString());
        updateProfileRequestModel.setInterest(interestsEditText.getText().toString());
        updateProfileRequestModel.setExperience(experienceEditText.getText().toString());
        updateProfileRequestModel.setCity(city);
        updateProfileRequestModel.setBirthDate(ageEditText.getText().toString());

        List<ContactInfoRequest> contactInfoLists = new ArrayList<>();
        if (skypeEditText.getText().length() != 0) {
            ContactInfoRequest contactInfoRequest = new ContactInfoRequest();
            contactInfoRequest.setType("Skype");
            contactInfoRequest.setInfo(skypeEditText.getText().toString());
            contactInfoLists.add(contactInfoRequest);

        }
        if (linkedInEditText.getText().length() != 0) {
            ContactInfoRequest contactInfoRequest = new ContactInfoRequest();
            contactInfoRequest.setType("Linkedin");
            contactInfoRequest.setInfo(linkedInEditText.getText().toString());
            contactInfoLists.add(contactInfoRequest);

        }
        if (facebookEditText.getText().length() != 0) {
            ContactInfoRequest contactInfoRequest = new ContactInfoRequest();
            contactInfoRequest.setType("Facebook");
            contactInfoRequest.setInfo(facebookEditText.getText().toString());
            contactInfoLists.add(contactInfoRequest);

        }
        if (twitterEditText.getText().length() != 0) {
            ContactInfoRequest contactInfoRequest = new ContactInfoRequest();
            contactInfoRequest.setType("Twitter");
            contactInfoRequest.setInfo(twitterEditText.getText().toString());
            contactInfoLists.add(contactInfoRequest);

        }
        updateProfileRequestModel.setContactInfoList(contactInfoLists);
        saveToSever(updateProfileRequestModel);


    }

    private void saveToSever(UpdateProfileRequestModel updateProfileRequestModel) {
        showProgress();
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            NetworkAdapter.getInstance(getContext()).updateProfile(updateProfileRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    hideProgress();
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }


    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showTopLayout() {
        containerLayout.setVisibility(View.VISIBLE);
    }

    private void hideTopLayout() {
        containerLayout.setVisibility(View.GONE);
    }

    private void getProfileDetails() {
        NetworkAdapter.getInstance(getContext()).getProfile(new ResponseCallback<GenericResponse<User>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<User> response) {
                if (response != null && response.getStatus() == Constants.RESPONSE_SUCCESS) {
                    UserUtils.setUserProfile(getContext(), response.getData());
                    hideProgress();
                    showTopLayout();
                    setData(response.getData());
                    setViewsAsNonEditable();
                }

            }

            @Override
            public void onFailure() {
                Log.w("OnFailure", "YUP");
            }
        });

    }

    private void setViewsAsNonEditable() {
        nameEditText.setEnabled(false);
        ageEditText.setEnabled(false);
        abilitiesEditText.setEnabled(false);
        emailIdEditText.setEnabled(false);
        mobileNumberEditText.setEnabled(false);
        aboutMeEditText.setEnabled(false);
        interestsEditText.setEnabled(false);
        skypeEditText.setEnabled(false);
        linkedInEditText.setEnabled(false);
        facebookEditText.setEnabled(false);
        twitterEditText.setEnabled(false);
        experienceEditText.setEnabled(false);
    }


    private void setViewsAsEditable() {
        nameEditText.setEnabled(true);
        ageEditText.setEnabled(true);
        emailIdEditText.setEnabled(true);
        mobileNumberEditText.setEnabled(true);
        aboutMeEditText.setEnabled(true);
        abilitiesEditText.setEnabled(true);
        interestsEditText.setEnabled(true);
        skypeEditText.setEnabled(true);
        linkedInEditText.setEnabled(true);
        facebookEditText.setEnabled(true);
        twitterEditText.setEnabled(true);
        experienceEditText.setEnabled(true);
    }

    private void setData(User data) {
        if (data.getFirstName() != null && data.getLastName() != null) {
            nameEditText.setText(data.getFirstName() + " " + data.getLastName());
        }
        if (data.getBirthDate() != null) {
            ageEditText.setText(data.getBirthDate());
        }
        emailIdEditText.setText(data.getEmail());
        if (data.getPhone() != null) {
            mobileNumberEditText.setText(data.getPhone());
        }

        if (data.getDescription() != null) {
            aboutMeEditText.setText(data.getDescription());
        }
        interestsEditText.setText("");
        if (data.getAbility() != null) {
            abilitiesEditText.setText(data.getAbility());
        }
        if (data.getInterest() != null) {
            interestsEditText.setText(data.getInterest());
        }

        if (data.getExperience() != null) {
            experienceEditText.setText(data.getExperience());
        }
        if (data.getProfilePic() != null) {
            Picasso.with(getContext()).load(data.getProfilePic()).into(profilePic);
        }
        if (data.getContactInfoList() != null) {
            for (int i = 0; i < data.getContactInfoList().size(); i++) {
                if (data.getContactInfoList().get(i).getType().equals("Skype")) {
                    skypeEditText.setText(data.getContactInfoList().get(i).getInfo());
                }
                if (data.getContactInfoList().get(i).getType().equals("Linkedin")) {
                    linkedInEditText.setText(data.getContactInfoList().get(i).getInfo());
                }
                if (data.getContactInfoList().get(i).getType().equals("Facebook")) {
                    facebookEditText.setText(data.getContactInfoList().get(i).getInfo());
                }
                if (data.getContactInfoList().get(i).getType().equals("Twitter")) {
                    twitterEditText.setText(data.getContactInfoList().get(i).getInfo());
                }

            }
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.profile_pic)
    public void onClick() {
        PickImage();
    }


    private void PickImage() {

        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .multi() // multi mode (default mode)
                .limit(1) // max images can be selected (999 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            File imgFile = new File(images.get(0).getPath());
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                profilePic.setImageBitmap(myBitmap);
                showProgress();
                updatePicToServer(imgFile);
            }
        }
    }

    private void updatePicToServer(File imgFile) {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            NetworkAdapter.getInstance(getContext()).postProfilePic(imgFile, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        Toast.makeText(getContext(), getString(R.string.profile_pic_is_updated_successfully), Toast.LENGTH_SHORT).show();
                        hideProgress();
                    }

                }

                @Override
                public void onFailure() {

                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void getAllCountries() {

        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgress();
            NetworkAdapter.getInstance(getContext()).getAllCountry(new ResponseCallback<GenericResponse<List<CountryModel>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<CountryModel>> response) {
                    setUpCountrySpinner(response);
                    hideProgress();
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
