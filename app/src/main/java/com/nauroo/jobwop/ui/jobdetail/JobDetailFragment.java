package com.nauroo.jobwop.ui.jobdetail;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AddJobRequestModel;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PageRefreshEvent;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.ui.jobdetail.viewapplicants.ViewApplicantActivity;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4="param4";
    @BindView(R.id.jobTittleTextView)
    CustomFontTextView jobTittleTextView;
    @BindView(R.id.applyTextView)
    CustomFontTextView applyTextView;
    @BindView(R.id.viewApplicantsTextView)
    CustomFontTextView viewApplicantsTextView;
    @BindView(R.id.descriptionTextView)
    CustomFontTextView descriptionTextView;
    @BindView(R.id.moneyTextView)
    CustomFontTextView moneyTextView;
    @BindView(R.id.periodicityTextView)
    CustomFontTextView periodicityTextView;
    @BindView(R.id.locationTextView)
    CustomFontTextView locationTextView;
    @BindView(R.id.workingHourTextView)
    CustomFontTextView workingHourTextView;
    @BindView(R.id.jobImageView)
    ImageView jobImageView;


    // TODO: Rename and change types of parameters
    private AllJobsResponseModel allJobsResponseModel;
    private boolean showViewApplicants;
    private boolean showApply;
    private boolean isAlreadyApplied;

    private OnFragmentInteractionListener mListener;

    public JobDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment JobDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobDetailFragment newInstance(AllJobsResponseModel param1, boolean showViewApplicant, boolean showApply, boolean isAlreadyApplied) {
        JobDetailFragment fragment = new JobDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, showViewApplicant);
        args.putBoolean(ARG_PARAM3, showApply);
        args.putBoolean(ARG_PARAM4,isAlreadyApplied);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            allJobsResponseModel = getArguments().getParcelable(ARG_PARAM1);
            showViewApplicants = getArguments().getBoolean(ARG_PARAM2);
            showApply = getArguments().getBoolean(ARG_PARAM3);
            isAlreadyApplied=getArguments().getBoolean(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job_detail, container, false);
        ButterKnife.bind(this, view);
        setData();
        return view;
    }

    private void setData() {
        try {

            if (showApply) {
                applyTextView.setVisibility(View.VISIBLE);
            } else {
                applyTextView.setVisibility(View.GONE);
            }
            if (isAlreadyApplied) {
                Log.w("isAlreadyApplied",allJobsResponseModel.isAlreadyApplied()+"");
                makeUIbuttonAsApplied();
            } else {
                makeUIButtonAsApply();
            }
            if (showViewApplicants) {
                viewApplicantsTextView.setVisibility(View.VISIBLE);
            } else {
                viewApplicantsTextView.setVisibility(View.GONE);
            }
            jobTittleTextView.setText(allJobsResponseModel.getName());
            descriptionTextView.setText(allJobsResponseModel.getDescription());
            moneyTextView.setText(getContext().getString(R.string.dollar) + "" + allJobsResponseModel.getRemuneration());
            periodicityTextView.setText(allJobsResponseModel.getPeriodicity());
            Picasso.with(getContext()).load(allJobsResponseModel.getImageUrl()).fit().into(jobImageView);
            workingHourTextView.setText(allJobsResponseModel.getSchedule());
            String location = allJobsResponseModel.getCity().getCity() + ", " + allJobsResponseModel.getCity().getCountry();
            locationTextView.setText(location);


        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    private void makeUIButtonAsApply() {
        applyTextView.setText(getString(R.string.apply));
        applyTextView.setTag(0);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            applyTextView.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.apply_button_background));
        } else {
            applyTextView.setBackground(getContext().getResources().getDrawable(R.drawable.apply_button_background));
        }
    }

    private void makeUIbuttonAsApplied() {
        applyTextView.setText(getString(R.string.applied));
        applyTextView.setTag(1);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            applyTextView.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.un_apply_button_background));
        } else {
            applyTextView.setBackground(getContext().getResources().getDrawable(R.drawable.un_apply_button_background));
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.applyTextView, R.id.viewApplicantsTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.applyTextView:

                addUserToJob();

                break;
            case R.id.viewApplicantsTextView:
                moveToViewApplicantActivity();
                break;
        }
    }

    private void moveToViewApplicantActivity() {
        Intent intent = new Intent(getActivity(), ViewApplicantActivity.class);
        intent.putExtra(Constants.EXTRA_PUBLISHED_JOB_ID, allJobsResponseModel.getId());
        getActivity().startActivity(intent);
    }


    private void addUserToJob() {

        int isApplied = Integer.valueOf(applyTextView.getTag().toString());
        if (isApplied == 0) {
            callAddUserToJobAPI();
        } else {
            new AlertDialog.Builder(getContext())
                    .setMessage(getContext().getString(R.string.are_you_sure_want_unapply_job))
                    .setCancelable(false)
                    .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            callRemoveUserToJobAPI();
                        }
                    })
                    .setNegativeButton(getContext().getString(R.string.no), null)
                    .show();

        }


    }


    private void callAddUserToJobAPI() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            applyTextView.setText(getContext().getString(R.string.loading));
            AddJobRequestModel addJobRequestModel = new AddJobRequestModel();
            addJobRequestModel.setId(allJobsResponseModel.getId());
            PageRefreshEvent.ITEM_ID = addJobRequestModel.getId();
            NetworkAdapter.getInstance(getContext()).addJobToUser(addJobRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        updateTextViewAsApplied();

                    }
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getContext().getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }


    }

    private void callRemoveUserToJobAPI() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            applyTextView.setText(getContext().getString(R.string.loading));
            AddJobRequestModel addJobRequestModel = new AddJobRequestModel();
            addJobRequestModel.setId(allJobsResponseModel.getId());
            PageRefreshEvent.ITEM_ID = addJobRequestModel.getId();
            NetworkAdapter.getInstance(getContext()).removeJob(addJobRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                        updateTextViewAsApply();

                    }
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getContext().getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }


    }

    private void updateTextViewAsApplied() {
        EventBus.getDefault().post(new PageRefreshEvent(1, PageRefreshEvent.ITEM_ID));
        makeUIbuttonAsApplied();


    }

    private void updateTextViewAsApply() {
        EventBus.getDefault().post(new PageRefreshEvent(0, PageRefreshEvent.ITEM_ID));
        makeUIButtonAsApply();


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
