package com.nauroo.jobwop.ui.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.ui.common.CustomFontTextView;
import com.nauroo.jobwop.ui.login.LoginActivity;
import com.nauroo.jobwop.ui.lookingforjob.applications.ApplicationsFragment;
import com.nauroo.jobwop.ui.lookingforjob.jobs.JobsFragment;
import com.nauroo.jobwop.ui.lookingforjob.myfavorites.FavoritesFragment;
import com.nauroo.jobwop.ui.lookingforjob.search.SearchFragment;
import com.nauroo.jobwop.ui.offerwork.postjob.PostJobFragment;
import com.nauroo.jobwop.ui.offerwork.publishedjob.PublishedJobsFragment;
import com.nauroo.jobwop.ui.profile.editprofile.EditProfileFragment;
import com.nauroo.jobwop.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.jobsTextView)
    CustomFontTextView jobsTextView;
    @BindView(R.id.searchTextView)
    CustomFontTextView searchTextView;
    @BindView(R.id.myApplicationTextView)
    CustomFontTextView myApplicationTextView;
    @BindView(R.id.myFavoritesTextView)
    CustomFontTextView myFavoritesTextView;
    @BindView(R.id.postJobTextView)
    CustomFontTextView postJobTextView;
    @BindView(R.id.publishedTextView)
    CustomFontTextView publishedTextView;
    @BindView(R.id.myProfileTextView)
    CustomFontTextView myProfileTextView;
    @BindView(R.id.logutTextView)
    CustomFontTextView logutTextView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance(String param1, String param2) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.jobsTextView, R.id.searchTextView, R.id.myApplicationTextView, R.id.myFavoritesTextView, R.id.postJobTextView, R.id.publishedTextView, R.id.myProfileTextView, R.id.logutTextView})
    public void onClick(View view) {
        Fragment fragment = null;
        String tittle="";
        switch (view.getId()) {
            case R.id.jobsTextView:
                fragment=new JobsFragment();
                tittle=getString(R.string.jobs);
                break;
            case R.id.searchTextView:
                fragment=new SearchFragment();
                tittle=getString(R.string.search);
                break;
            case R.id.myApplicationTextView:
                fragment=new ApplicationsFragment();
                tittle=getString(R.string.applications);
                break;
            case R.id.myFavoritesTextView:
                fragment=new FavoritesFragment();
                tittle=getString(R.string.my_favorites);
                break;
            case R.id.postJobTextView:
                fragment=new PostJobFragment();
                tittle=getString(R.string.post_a_job);
                break;
            case R.id.publishedTextView:
                fragment=new PublishedJobsFragment();
                tittle=getString(R.string.published_works);
                break;
            case R.id.myProfileTextView:
                fragment=new EditProfileFragment();
                tittle=getString(R.string.my_profile);
                break;
            case R.id.logutTextView:
                new AlertDialog.Builder(getActivity())
                        .setMessage(getResources().getString(R.string.are_you_sure_want_to_logout))
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                doLogout();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), null)
                        .show();
                break;
        }
        if (fragment != null) {
            switchFragment(fragment,tittle);
        }
    }

    private void doLogout() {
        PreferenceUtil.clearPreference(getActivity());
        Intent intent = new Intent(getContext(), LoginActivity.class);
        getActivity().finish();
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void switchFragment(Fragment fragment,String  title) {
        if (getActivity() == null)
            return;
        if (getActivity() instanceof HomeActivity) {
            HomeActivity ra = (HomeActivity) getActivity();
            ra.switchContent(fragment,title);
        }
    }
}
