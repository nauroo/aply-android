package com.nauroo.jobwop.ui.lookingforjob.search;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nauroo.jobwop.R;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PageRefreshEvent;
import com.nauroo.jobwop.model.SearchRequestKeyword;
import com.nauroo.jobwop.model.UpdateCityModel;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.ui.common.CustomEditText;
import com.nauroo.jobwop.ui.lookingforjob.jobs.JobsAdapter;
import com.nauroo.jobwop.utils.Constants;
import com.nauroo.jobwop.utils.InternetAvailability;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.searchResultRecyclerView)
    RecyclerView searchResultRecyclerView;
    @BindView(R.id.searchEditText)
    CustomEditText searchEditText;
    @BindView(R.id.searchImageViewButton)
    ImageView searchImageViewButton;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    volatile int check = 0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    List<City> cityList;
    List<AllJobsResponseModel> jobs;
    JobsAdapter jobsAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        getActivity().findViewById(R.id.actionTextView).setVisibility(View.GONE);
        setUpCitySpinner();
//        setUpSearchRecyclerView();
        hideBottomResultLayout();
        hideSpinner();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.w("Executed", "YES");
                try {

                    if (check > 0) {
                        updateUserCity(cityList.get(spinner.getSelectedItemPosition()));
                        view.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
                        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        Log.w("RequestId", cityList.get(spinner.getSelectedItemPosition()).getId());
                        List<AllJobsResponseModel> sortedList = new ArrayList<AllJobsResponseModel>();
                        for (int i = 0; i < jobs.size(); i++) {
                            if (cityList.get(spinner.getSelectedItemPosition()).getId().equals(jobs.get(i).getCity().getId())) {
                                sortedList.add(jobs.get(i));
                            }
                        }
                        if (sortedList.size() == 0) {
                            String message = getString(R.string.no_jobs_found_at) + " " + cityList.get(spinner.getSelectedItemPosition()).getCity();
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                        }
                        updateFilterSearch(sortedList);

                    }
                    check++;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    doSearch();
                    return true;
                }
                return false;
            }
        });

        return view;
    }

    private void updateUserCity(City city) {
        UpdateCityModel updateCityModel = new UpdateCityModel();
        updateCityModel.setCity(city.getCity());
        updateCityModel.setId(city.getId());
        updateCityModel.setCountry(city.getCountry());
        NetworkAdapter.getInstance(getContext()).updateCity(updateCityModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<String> response) {

            }

            @Override
            public void onFailure() {

            }
        });
    }

    private void hideBottomResultLayout() {
        searchResultRecyclerView.setVisibility(View.GONE);
    }

    private void showBottomResultLayout() {
              searchResultRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showSpinner() {
        spinner.setVisibility(View.VISIBLE);
    }

    private void hideSpinner() {
        spinner.setVisibility(View.GONE);
    }


    private void setUpCitySpinner() {

        if (InternetAvailability.isNetworkAvailable(getContext())) {
            NetworkAdapter.getInstance(getContext()).getCity(new ResponseCallback<GenericResponse<List<City>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<List<City>> response) {
                    cityList = response.getData();
                    ArrayList<String> filterString = new ArrayList<>();
                    for (int i = 0; i < response.getData().size(); i++) {
                        filterString.add(response.getData().get(i).getCity());
                    }
                    CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), R.layout.search_filter_layout, filterString);
                    spinner.setAdapter(customSpinnerAdapter);
                    spinner.setSelection(0, true);

                }

                @Override
                public void onFailure() {

                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_SHORT).show();
        }

    }

    private void updateFilterSearch(List<AllJobsResponseModel> jobsResponseModelList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        searchResultRecyclerView.setLayoutManager(layoutManager);
        jobsAdapter = new JobsAdapter(getContext(), jobsResponseModelList);
        searchResultRecyclerView.setAdapter(jobsAdapter);
        searchResultRecyclerView.setNestedScrollingEnabled(false);
    }


    private void setUpSearchRecyclerView(List<AllJobsResponseModel> jobsResponseModelList) {
        jobs = jobsResponseModelList;
        Log.w("Search result size", jobsResponseModelList.size() + "");
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        searchResultRecyclerView.setLayoutManager(layoutManager);
        jobsAdapter = new JobsAdapter(getContext(), jobsResponseModelList);
        searchResultRecyclerView.setAdapter(jobsAdapter);
        searchResultRecyclerView.setNestedScrollingEnabled(false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.searchImageViewButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.searchImageViewButton:
                doSearch();
                break;

        }
    }

    private void doSearch() {
        if (searchEditText.getText().length() == 0) {
            searchEditText.setError(getString(R.string.search_string_should_not_be_empty));
        } else {
            if (InternetAvailability.isNetworkAvailable(getContext())) {
                callSearchAPI();
            } else {
                Toast.makeText(getContext(), getString(R.string.internet_connectivity_is_not_available), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void callSearchAPI() {
        showProgress();
        String[] keywords = searchEditText.getText().toString().split(",");
        List<SearchRequestKeyword> searchRequestKeywordArrayList = new ArrayList<>();
        for (int i = 0; i < keywords.length; i++) {
            SearchRequestKeyword searchRequestKeyword = new SearchRequestKeyword();
            searchRequestKeyword.setKeyword(keywords[i]);
            searchRequestKeywordArrayList.add(searchRequestKeyword);
        }
        NetworkAdapter.getInstance(getContext()).searchJob(searchRequestKeywordArrayList, new ResponseCallback<GenericResponse<List<AllJobsResponseModel>>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<List<AllJobsResponseModel>> response) {
                if (response.getStatus() == Constants.RESPONSE_SUCCESS) {
                    if (response.getData() != null) {
                        if (response.getData().size() == 0) {
                            Toast.makeText(getContext(), getString(R.string.no_jobs_found), Toast.LENGTH_SHORT).show();
                            hideProgress();
                        } else {
                            hideProgress();
                            showBottomResultLayout();
                            setUpSearchRecyclerView(response.getData());
                            showSpinner();
                        }

                    }
                }
            }

            @Override
            public void onFailure() {

            }
        });
    }

    @Subscribe
    public void onMessageEvent(PageRefreshEvent event) {
        Log.w("Executed","YES");
        if (event.getType() == 1) {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(true);
                    jobsAdapter.notifyItemChanged(i);
                }

            }
        } else {
            for (int i = 0; i < jobs.size(); i++) {
                if (jobs.get(i).getId().equals(event.getId())) {
                    jobs.get(i).setAlreadyApplied(false);
                    jobsAdapter.notifyItemChanged(i);
                }

            }
        }
    }

    ;

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
