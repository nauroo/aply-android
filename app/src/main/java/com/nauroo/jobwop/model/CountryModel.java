package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 8/17/2017.
 */

public class CountryModel implements Parcelable{
    public String getCountry() {
        return country;
    }

    String country;

    protected CountryModel(Parcel in) {
        country = in.readString();
    }

    public static final Creator<CountryModel> CREATOR = new Creator<CountryModel>() {
        @Override
        public CountryModel createFromParcel(Parcel in) {
            return new CountryModel(in);
        }

        @Override
        public CountryModel[] newArray(int size) {
            return new CountryModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(country);
    }
}
