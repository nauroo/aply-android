package com.nauroo.jobwop.model;

public class AuthenticationResponseModel {
    String access_token;
    String refresh_token;

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getAccessToken() {
        return access_token;
    }
}
