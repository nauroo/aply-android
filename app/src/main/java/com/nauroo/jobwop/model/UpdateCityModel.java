package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 8/7/2017.
 */

public class UpdateCityModel {
    String id;
    String city;
    String country;

    public void setId(String id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
