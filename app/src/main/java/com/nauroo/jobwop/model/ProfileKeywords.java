package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 8/2/2017.
 */

public class ProfileKeywords implements Parcelable {
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    String keyword;

    protected ProfileKeywords(Parcel in) {
        keyword = in.readString();
    }

    public static final Creator<ProfileKeywords> CREATOR = new Creator<ProfileKeywords>() {
        @Override
        public ProfileKeywords createFromParcel(Parcel in) {
            return new ProfileKeywords(in);
        }

        @Override
        public ProfileKeywords[] newArray(int size) {
            return new ProfileKeywords[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(keyword);
    }
}
