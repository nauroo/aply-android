package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 8/2/2017.
 */

public class ContactInfoRequest {
    String type;
    String info;

    public void setType(String type) {
        this.type = type;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
