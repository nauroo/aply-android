package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class CreatedById implements Parcelable {

    String timestamp;
    String machineIdentifier;
    String processIdentifier;
    String counter;
    String time;
    String date;
    String timeSecond;

    protected CreatedById(Parcel in) {
        timestamp = in.readString();
        machineIdentifier = in.readString();
        processIdentifier = in.readString();
        counter = in.readString();
        time = in.readString();
        date = in.readString();
        timeSecond = in.readString();
    }

    public static final Creator<CreatedById> CREATOR = new Creator<CreatedById>() {
        @Override
        public CreatedById createFromParcel(Parcel in) {
            return new CreatedById(in);
        }

        @Override
        public CreatedById[] newArray(int size) {
            return new CreatedById[size];
        }
    };

    public String getTimestamp() {
        return timestamp;
    }

    public String getMachineIdentifier() {
        return machineIdentifier;
    }

    public String getProcessIdentifier() {
        return processIdentifier;
    }

    public String getCounter() {
        return counter;
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public String getTimeSecond() {
        return timeSecond;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(timestamp);
        parcel.writeString(machineIdentifier);
        parcel.writeString(processIdentifier);
        parcel.writeString(counter);
        parcel.writeString(time);
        parcel.writeString(date);
        parcel.writeString(timeSecond);
    }
}
