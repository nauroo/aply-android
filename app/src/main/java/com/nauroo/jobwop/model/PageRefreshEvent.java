package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 8/4/2017.
 */

public class PageRefreshEvent {
    public static String ITEM_ID;
    private int type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    public PageRefreshEvent(int type, String id) {
        this.type = type;
        this.id=id;
    }

    public int getType() {
        return type;
    }
}
