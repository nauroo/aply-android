package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class RegistrationRequestModel {
    String email;
    String password;

    public void setCity(CityRequestModel city) {
        this.city = city;
    }

    CityRequestModel city;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
