package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class Keyword implements Parcelable{

    String keyword;

    protected Keyword(Parcel in) {
        keyword = in.readString();
    }

    public static final Creator<Keyword> CREATOR = new Creator<Keyword>() {
        @Override
        public Keyword createFromParcel(Parcel in) {
            return new Keyword(in);
        }

        @Override
        public Keyword[] newArray(int size) {
            return new Keyword[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(keyword);
    }
}
