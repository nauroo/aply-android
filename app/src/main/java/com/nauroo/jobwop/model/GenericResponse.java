package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class GenericResponse<T> {
    private T response;
    private int code;

    public int getStatus() {
        return code;
    }

    public T getData() {
        return response;
    }
}
