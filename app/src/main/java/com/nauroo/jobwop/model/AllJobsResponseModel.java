package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class AllJobsResponseModel implements Parcelable{

    private String id;
    private String name;
    //private String address;
    private City city;
 //   private CreatedById createdById;
  //  private CreatedBy createdBy;

//    public CreatedBy getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(CreatedBy createdBy) {
//        this.createdBy = createdBy;
//    }

    private String shortDescription;
    private String description;
    private String periodicity;
    private  String remuneration;
    private String imageUrl;
    private List<Keyword> keywords=new ArrayList<>();

    public int getApplicantsCount() {
        return applicantsCount;
    }

    public void setApplicantsCount(int applicantsCount) {
        this.applicantsCount = applicantsCount;
    }

    private int applicantsCount;



    private boolean  alreadyApplied;
    private boolean isFavorite;
    private  String schedule;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

//    public CreatedById getCreatedById() {
//        return createdById;
//    }
//
//    public void setCreatedById(CreatedById createdById) {
//        this.createdById = createdById;
//    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public String getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(String remuneration) {
        this.remuneration = remuneration;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Keyword> getKeyword() {
        return keywords;
    }

    public void setKeyword(List<Keyword> keywords) {
        this.keywords = keywords;
    }


    public boolean isAlreadyApplied() {
        return alreadyApplied;
    }

    public void setAlreadyApplied(boolean alreadyApplied) {
        this.alreadyApplied = alreadyApplied;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    protected AllJobsResponseModel(Parcel in) {
        id = in.readString();
        name = in.readString();
       // address = in.readString();
        shortDescription = in.readString();
        description = in.readString();
        periodicity = in.readString();
        remuneration = in.readString();
        imageUrl = in.readString();
        keywords = in.createTypedArrayList(Keyword.CREATOR);
        alreadyApplied = in.readByte() != 0;
        isFavorite = in.readByte() != 0;
        schedule = in.readString();
        city=in.readParcelable(City.class.getClassLoader());
     //   createdById=in.readParcelable(CreatedById.class.getClassLoader());
        applicantsCount=in.readInt();
        //createdBy=in.readParcelable(CreatedBy.class.getClassLoader());
    }
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        // parcel.writeString(address);
        parcel.writeString(shortDescription);
        parcel.writeString(description);
        parcel.writeString(periodicity);
        parcel.writeString(remuneration);
        parcel.writeString(imageUrl);

        parcel.writeTypedList(keywords);
        // parcel.writeParcelable(createdBy,i);
        parcel.writeByte((byte) (alreadyApplied ? 1 : 0));
        parcel.writeByte((byte) (isFavorite ? 1 : 0));
        parcel.writeString(schedule);
        parcel.writeParcelable(city,i);
        //   parcel.writeParcelable(createdById,i);
        parcel.writeInt(applicantsCount);
    }
    public static final Creator<AllJobsResponseModel> CREATOR = new Creator<AllJobsResponseModel>() {
        @Override
        public AllJobsResponseModel createFromParcel(Parcel in) {
            return new AllJobsResponseModel(in);
        }

        @Override
        public AllJobsResponseModel[] newArray(int size) {
            return new AllJobsResponseModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


}
