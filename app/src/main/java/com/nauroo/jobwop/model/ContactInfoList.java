package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 8/2/2017.
 */

public class ContactInfoList implements Parcelable {
    protected ContactInfoList(Parcel in) {
        type = in.readString();
        info = in.readString();
    }

    public static final Creator<ContactInfoList> CREATOR = new Creator<ContactInfoList>() {
        @Override
        public ContactInfoList createFromParcel(Parcel in) {
            return new ContactInfoList(in);
        }

        @Override
        public ContactInfoList[] newArray(int size) {
            return new ContactInfoList[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    String type;
    String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
        parcel.writeString(info);
    }
}
