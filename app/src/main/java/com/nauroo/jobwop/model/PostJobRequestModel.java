package com.nauroo.jobwop.model;

import java.util.List;

/**
 * Created by Mohan M on 8/1/2017.
 */

public class PostJobRequestModel {
    String name;
    String description;
    City city;
    String remuneration;
    String periodicity;
    List<KeywordRequest> keywords;

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    String schedule;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getRemuneration() {
        return remuneration;
    }

    public void setRemuneration(String remuneration) {
        this.remuneration = remuneration;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public List<KeywordRequest >getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordRequest> keywords) {
        this.keywords = keywords;
    }
}
