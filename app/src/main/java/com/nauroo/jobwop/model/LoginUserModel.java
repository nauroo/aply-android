package com.nauroo.jobwop.model;

public class LoginUserModel {

    private String password;
    private String username;

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    private String grant_type = "password";
    private String scope = "read write";
    private String client_secret = "naurooJobsSecret";
    private String client_id = "naurooJobs";
    private String refresh_token;

    public String getGrandTypePassword() {
        return grandTypePassword;
    }

    public void setGrandTypePassword(String grandTypePassword) {
        this.grandTypePassword = grandTypePassword;
    }

    private String grandTypePassword;
    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getScope() {
        return scope;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGrantTypeRefresh() {
        this.grant_type = "refresh_token";
    }
}
