package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 8/1/2017.
 */

public class CityRequestModel {
    String id;
    String city;
    String country;

    public String getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
