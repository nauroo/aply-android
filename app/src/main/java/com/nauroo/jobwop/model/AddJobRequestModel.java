package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 7/28/2017.
 */

public class AddJobRequestModel {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    String id;
}
