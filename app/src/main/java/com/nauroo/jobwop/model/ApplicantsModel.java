package com.nauroo.jobwop.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohan M on 8/4/2017.
 */

public class ApplicantsModel implements Parcelable{
    String id;
    String firstName;
    String lastName;
    String email;
    String password;
    String profilePic;
    String description;

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    String ability;
    String interest;

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    String experience;
    CreatedById createdById;
    List<ContactInfoList> contactInfoList=new ArrayList<>();
    String address;
    //String city;
    String username;
    boolean enabled;
    boolean authenticated;
    boolean credentialsNonExpired;
    boolean accountNonLocked;
    String age;
    String phone;

    public CreatedById getCreatedById() {
        return createdById;
    }



    protected ApplicantsModel(Parcel in) {
        id = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        password = in.readString();
        profilePic = in.readString();
        description = in.readString();
        ability = in.readString();
        interest = in.readString();
        experience=in.readString();
        contactInfoList = in.createTypedArrayList(ContactInfoList.CREATOR);
        address = in.readString();
//        city = in.readString();
        username = in.readString();
        enabled = in.readByte() != 0;
        authenticated = in.readByte() != 0;
        credentialsNonExpired = in.readByte() != 0;
        accountNonLocked = in.readByte() != 0;
        age = in.readString();
        phone = in.readString();
        accountNonExpired = in.readByte() != 0;
        createdById=in.readParcelable(CreatedById.class.getClassLoader());
    }

    public static final Creator<ApplicantsModel> CREATOR = new Creator<ApplicantsModel>() {
        @Override
        public ApplicantsModel createFromParcel(Parcel in) {
            return new ApplicantsModel(in);
        }

        @Override
        public ApplicantsModel[] newArray(int size) {
            return new ApplicantsModel[size];
        }
    };


    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public List<ContactInfoList> getContactInfoList() {
        return contactInfoList;
    }

    public void setContactInfoList(List<ContactInfoList> contactInfoList) {
        this.contactInfoList = contactInfoList;
    }



    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    boolean accountNonExpired;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(profilePic);
        parcel.writeString(description);
        parcel.writeString(ability);
        parcel.writeString(interest);
        parcel.writeString(experience);
        parcel.writeTypedList(contactInfoList);
        parcel.writeString(address);
        //parcel.writeString(city);
        parcel.writeString(username);
        parcel.writeByte((byte) (enabled ? 1 : 0));
        parcel.writeByte((byte) (authenticated ? 1 : 0));
        parcel.writeByte((byte) (credentialsNonExpired ? 1 : 0));
        parcel.writeByte((byte) (accountNonLocked ? 1 : 0));
        parcel.writeString(age);
        parcel.writeString(phone);
        parcel.writeByte((byte) (accountNonExpired ? 1 : 0));
        parcel.writeParcelable(createdById,i);
    }
}
