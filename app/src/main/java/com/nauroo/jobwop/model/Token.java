package com.nauroo.jobwop.model;

/**
 * Created by Mohan M on 11/12/2016.
 */

public class Token {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
