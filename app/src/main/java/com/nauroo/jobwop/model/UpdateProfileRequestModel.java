package com.nauroo.jobwop.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohan M on 8/2/2017.
 */

public class UpdateProfileRequestModel {
    String firstName;
    String lastName;
    String description;
    List<ProfileUpdateKeywords> keywords=new ArrayList<>();
    String phone;
    String interest;

    public String getBirthDate() {

        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    String birthDate;
    public void setAbility(String ability) {
        this.ability = ability;
    }

    String ability;

    public void setCity(CityRequestModel city) {
        this.city = city;
    }

    CityRequestModel city;

    public void setExperience(String experience) {
        this.experience = experience;
    }


    String experience;


    public void setInterest(String interest) {
        this.interest = interest;
    }

    List<ContactInfoRequest> contactInfoList=new ArrayList<>();



    public void setContactInfoList(List<ContactInfoRequest> contactInfoList) {
        this.contactInfoList = contactInfoList;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setKeywords(List<ProfileUpdateKeywords> keywords) {
        this.keywords = keywords;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
