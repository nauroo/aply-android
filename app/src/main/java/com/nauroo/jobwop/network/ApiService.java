package com.nauroo.jobwop.network;


import com.nauroo.jobwop.model.AddJobRequestModel;
import com.nauroo.jobwop.model.AddToFavRequestModel;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.ApplicantsModel;
import com.nauroo.jobwop.model.AuthenticationResponseModel;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.CityRequestModel;
import com.nauroo.jobwop.model.CountryModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.PostJobRequestModel;
import com.nauroo.jobwop.model.RegistrationRequestModel;
import com.nauroo.jobwop.model.SearchRequestKeyword;
import com.nauroo.jobwop.model.Token;
import com.nauroo.jobwop.model.UpdateCityModel;
import com.nauroo.jobwop.model.UpdateProfileRequestModel;
import com.nauroo.jobwop.model.User;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiService {
    @FormUrlEncoded
    @POST("oauth/token")
    Call<AuthenticationResponseModel> getAccessToken(@Field("password") String password,
                                                     @Field("username") String username,
                                                     @Field("grant_type") String grant_type,
                                                     @Field("scope") String scope,
                                                     @Field("client_secret") String clientSecret,
                                                     @Field("client_id") String clientId,
                                                     @Field("refresh_token") String refreshToken);
    @POST("user/add")
    Call<GenericResponse<String>> register(@Body RegistrationRequestModel userRequest);

    @GET("job/find/all")
    Call<GenericResponse<List<AllJobsResponseModel>>> getAllJobs();

    @POST("user/job/add")
    Call<GenericResponse<String>> addJobToUser(@Body AddJobRequestModel addJobRequestModel);

    @GET("/user/profile")
    Call<GenericResponse<User>> getProfile();

    @POST("job/search")
    Call<GenericResponse<List<AllJobsResponseModel>>> search(@Body List<SearchRequestKeyword> searchRequestKeywords);

    @GET("user/jobs/favorite")
    Call<GenericResponse<List<AllJobsResponseModel>>> getFavorite();

    @Multipart
    @POST("job/add")
    Call<GenericResponse<String>> postJob(@Part MultipartBody.Part filedata, @Part("object") PostJobRequestModel postJobRequestModel);

    @POST("user/job/favorite")
    Call<GenericResponse<String>> addFavorite(@Body AddToFavRequestModel addToFavRequestModel);

    @GET("city/find/all")
    Call<GenericResponse<List<City>>> getCity();

    @Multipart
    @POST("/user/profile/picture")
    Call<GenericResponse<String>> postProfilePic(@Part("filename") RequestBody filename, @Part MultipartBody.Part filedata);

    @PUT("user/update")
    Call<GenericResponse<String>> updateProfile(@Body UpdateProfileRequestModel updateProfileRequestModel);

    @GET("publisher/jobs/created")
    Call<GenericResponse<List<AllJobsResponseModel>>> getPublishedJobs();

    @GET("job/find/applicants/{id}")
    Call<GenericResponse<List<ApplicantsModel>>> getApplicantlist(@Path(value = "id") String userId);

    @GET("user/jobs/applied")
    Call<GenericResponse<List<AllJobsResponseModel>>> getAppliedJobs();

    @PUT("user/city/update")
    Call<GenericResponse<String>> updateCity(@Body UpdateCityModel updateCityModel);

    @POST("user/job/remove")
    Call<GenericResponse<String>> removeJob(@Body AddJobRequestModel addJobRequestModel);

    @GET("country/find/all")
    Call<GenericResponse<List<CountryModel>>> getAllCountry();

    @POST("city/find/country")
    Call<GenericResponse<List<City>>> getCityOfParticularCountry(@Body CityRequestModel cityRequestModel);


    @POST("user/device/android/add")
    Call<GenericResponse<String>> sendToken(@Body Token token);
}
