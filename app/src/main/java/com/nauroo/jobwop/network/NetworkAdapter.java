package com.nauroo.jobwop.network;

import android.content.Context;
import android.util.Log;

import com.nauroo.jobwop.model.AddJobRequestModel;
import com.nauroo.jobwop.model.AddToFavRequestModel;
import com.nauroo.jobwop.model.AllJobsResponseModel;
import com.nauroo.jobwop.model.ApplicantsModel;
import com.nauroo.jobwop.model.AuthenticationResponseModel;
import com.nauroo.jobwop.model.City;
import com.nauroo.jobwop.model.CityRequestModel;
import com.nauroo.jobwop.model.CountryModel;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.LoginUserModel;
import com.nauroo.jobwop.model.PostJobRequestModel;
import com.nauroo.jobwop.model.RegistrationRequestModel;
import com.nauroo.jobwop.model.SearchRequestKeyword;
import com.nauroo.jobwop.model.Token;
import com.nauroo.jobwop.model.UpdateCityModel;
import com.nauroo.jobwop.model.UpdateProfileRequestModel;
import com.nauroo.jobwop.model.User;
import com.nauroo.jobwop.utils.Utils;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Mohan M on 7/27/2017.
 */

public class NetworkAdapter {

    private static NetworkAdapter INSTANCE = null;
    private Context mContext;
    private ApiService mApiService;

    public static NetworkAdapter getInstance(Context context) {
        if (null == INSTANCE) {
            INSTANCE = new NetworkAdapter(context);
        }
        return INSTANCE;
    }

    private NetworkAdapter(Context context) {
        mContext = context;
        mApiService = new RestClient(context).getService();
    }

    public void login(LoginUserModel loginUserModel, Callback<AuthenticationResponseModel> callback) {
        Call<AuthenticationResponseModel> call = mApiService.getAccessToken(loginUserModel.getPassword(),
                loginUserModel.getUsername(),
                loginUserModel.getGrant_type(),
                loginUserModel.getScope(),
                loginUserModel.getClient_secret(),
                loginUserModel.getClient_id(), Utils.getRefreshToken(mContext));
        call.enqueue(callback);

    }
    public void registerUser(RegistrationRequestModel registrationRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.register(registrationRequestModel);
        call.enqueue(responseCallback);
    }

    public void getAllJobs(ResponseCallback<GenericResponse<List<AllJobsResponseModel>>> allJobsResponseModelGenericResponse) {
        Call<GenericResponse<List<AllJobsResponseModel>>> call = mApiService.getAllJobs();
        call.enqueue(allJobsResponseModelGenericResponse);
    }
    public void addJobToUser(AddJobRequestModel addJobRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.addJobToUser(addJobRequestModel);
        call.enqueue(responseCallback);
    }

    public void getProfile(ResponseCallback<GenericResponse<User>> responseCallback) {
        Call<GenericResponse<User>> call = mApiService.getProfile();
        call.enqueue(responseCallback);
    }

    public void searchJob(List<SearchRequestKeyword> searchRequestKeywords, ResponseCallback<GenericResponse<List<AllJobsResponseModel>>> responseCallback) {
        Call<GenericResponse<List<AllJobsResponseModel>>> call = mApiService.search(searchRequestKeywords);
        call.enqueue(responseCallback);
    }
    public void getFavorites(ResponseCallback<GenericResponse<List<AllJobsResponseModel>>> genericResponseResponseCallback) {
        Call<GenericResponse<List<AllJobsResponseModel>>> call = mApiService.getFavorite();
        call.enqueue(genericResponseResponseCallback);
    }

    public void postJob(File file, PostJobRequestModel postJobRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {


        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), file.getName());
        Call<GenericResponse<String>> call = mApiService.postJob(body,postJobRequestModel);
        call.enqueue(responseCallback);
    }

    public void addToFavJob(AddToFavRequestModel addToFavRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.addFavorite(addToFavRequestModel);
        call.enqueue(responseCallback);
    }
    public void getCity(ResponseCallback<GenericResponse<List<City>>> genericResponseResponseCallback) {
        Call<GenericResponse<List<City>>> call = mApiService.getCity();
        call.enqueue(genericResponseResponseCallback);
    }

    public void postProfilePic(File file, ResponseCallback<GenericResponse<String>> responseCallback) {


        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("filedata", file.getName(), requestFile);
        RequestBody fileName = RequestBody.create(MediaType.parse("multipart/form-data"), file.getName());
        Log.w("File Name",fileName.toString());
        Log.w("body",body.toString());
        Call<GenericResponse<String>> call = mApiService.postProfilePic(fileName, body);
        call.enqueue(responseCallback);
    }

    public void updateProfile(UpdateProfileRequestModel updateProfileRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.updateProfile(updateProfileRequestModel);
        call.enqueue(responseCallback);
    }
    public void getPublishedJobs(ResponseCallback<GenericResponse<List<AllJobsResponseModel>>> genericResponseResponseCallback) {
        Call<GenericResponse<List<AllJobsResponseModel>>> call = mApiService.getPublishedJobs();
        call.enqueue(genericResponseResponseCallback);
    }
    public void getApplicantList(String id,ResponseCallback<GenericResponse<List<ApplicantsModel>>> responseResponseCallback) {
        Call<GenericResponse<List<ApplicantsModel>>> call = mApiService.getApplicantlist(id);
        call.enqueue(responseResponseCallback);
    }
    public void getAppliedJobs(ResponseCallback<GenericResponse<List<AllJobsResponseModel>>> genericResponseResponseCallback) {
        Call<GenericResponse<List<AllJobsResponseModel>>> call = mApiService.getAppliedJobs();
        call.enqueue(genericResponseResponseCallback);
    }

    public void updateCity(UpdateCityModel updateCityModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.updateCity(updateCityModel);
        call.enqueue(responseCallback);
    }

    public void removeJob(AddJobRequestModel addJobRequestModel, ResponseCallback<GenericResponse<String>> responseCallback) {
        Call<GenericResponse<String>> call = mApiService.removeJob(addJobRequestModel);
        call.enqueue(responseCallback);
    }

    public void getAllCountry(ResponseCallback<GenericResponse<List<CountryModel>>> genericResponseResponseCallback) {
        Call<GenericResponse<List<CountryModel>>> call = mApiService.getAllCountry();
        call.enqueue(genericResponseResponseCallback);
    }

    public void getCityOfParticularCountry(CityRequestModel cityRequestModel, ResponseCallback<GenericResponse<List<City>>> responseCallback) {
        Call<GenericResponse<List<City>>> call = mApiService.getCityOfParticularCountry(cityRequestModel);
        call.enqueue(responseCallback);
    }

    public void Token(Token token, ResponseCallback<GenericResponse<String>> tokenResponse) {
        Call<GenericResponse<String>> call = mApiService.sendToken(token);
        call.enqueue(tokenResponse);
    }

}
