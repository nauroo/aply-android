package com.nauroo.jobwop.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.nauroo.jobwop.model.GenericResponse;
import com.nauroo.jobwop.model.Token;
import com.nauroo.jobwop.network.NetworkAdapter;
import com.nauroo.jobwop.network.ResponseCallback;
import com.nauroo.jobwop.utils.Utils;

/**
 * Created by Mohan M on 11/12/2016.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Token token1=new Token();
        token1.setToken(token);
        if (Utils.isOnline(getApplicationContext())) {
            NetworkAdapter.getInstance(getApplicationContext()).Token(token1,
                    new ResponseCallback<GenericResponse<String>>(getApplicationContext()) {
                        @Override
                        public void onResponse(GenericResponse<String> response) {


                        }

                        @Override
                        public void onFailure() {

                        }
                    });
        }
        // TODO: Implement this method to send token to your app server.
    }
}