package com.nauroo.jobwop.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

public class Utils {
    public static String UUID_OF_SITTER;

    public static void setAccessToken(Context context, String accessToken) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_ACCESS_TOKEN, accessToken);
        }
    }

    public static String getAccessToken(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_ACCESS_TOKEN);
        }
        return null;
    }


    public static boolean isOnline(Context context) {
        if (context != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return false;
    }

    public static void setEmailId(Context context, String userName) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_PHONE_NUMBER, userName);
        }
    }

    public static String getEmailId(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_PHONE_NUMBER);
        }
        return null;
    }

    public static void setUserPassword(Context context, String password) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_USER_PASSWORD, password);
        }
    }

    public static String getUserPassword(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_USER_PASSWORD);
        }
        return null;
    }

    public static void setRefreshToken(Context context, String refresh_token) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_REFRESH_TOKEN, refresh_token);
        }
    }

    public static String getRefreshToken(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_REFRESH_TOKEN);
        }
        return null;
    }

    public static boolean isLoggedIn(Context context) {
        if (context != null) {
            return !TextUtils.isEmpty(UserUtils.getUserId(context));
        }
        return false;
    }


    public static void callNumber(Context context, String number) {
        if (context != null) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                context.startActivity(intent);
            }
        }
    }

    public static void dialNumber(Context context, String number) {
        if (context != null) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
            context.startActivity(intent);
        }
    }


}
