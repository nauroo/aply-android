package com.nauroo.jobwop.utils;


public class Constants {

    //Preference Keys
    public static final String SP_ACCESS_TOKEN = "sp_access_token";
    public static final String SP_PHONE_NUMBER = "sp_phone_number";
    public static final String SP_USER_PASSWORD = "sp_user_password";
    public static final String SP_REFRESH_TOKEN = "sp_refresh_token";
    public static final String SP_USER_ID = "sp_user_id";
    public static final String SP_USER_PROFILE = "sp_user_profile";
    public static final String SP_IN_PROGRESS_BOOKING = "in_progress_booking";

    //Network API response codes
    public static final int RESPONSE_SUCCESS=200;
    public static final int USER_ALREADY_EXIST=201;

    //Intent Extras
    public static final String EXTRA_JOBS="extra_jobs";
    public static final String EXTRA_PUBLISHED_JOB_ID ="extra_published_job_id" ;
    public static final String EXTRA_SHOW_VIEW_APPLICANTS="extra_show_view_applicants";
    public static final String EXTRA_SHOW_APPLY_TEXT="extra_show_apply";
    public static final String EXTRA_USER="extra_user";
   public static  boolean EXTRA_IS_ALREADY_APPLIED;
}
